"""byway_url URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from url_shorter import views as url_shorter_views

urlpatterns = [
	path('admin/', admin.site.urls),
	path('_api/', include('url_shorter.urls')),
	path('_accounts/', include('accounts.urls')),
	re_path(r'^(?P<base62_id>[a-zA-Z0-9]+)/$', url_shorter_views.ShorterRedirect.as_view(), name='shorter.redirect'),
]
