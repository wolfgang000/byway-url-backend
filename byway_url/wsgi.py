"""
WSGI config for byway_url project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "byway_url.settings")
application = get_wsgi_application()

if os.environ.get('WHITENOISE_ENABLE'):
	import ast
	if ast.literal_eval(os.environ.get('WHITENOISE_ENABLE')):
		from whitenoise.django import DjangoWhiteNoise
		application = DjangoWhiteNoise(application)