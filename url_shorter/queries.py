from django.db import models, connection
from collections import namedtuple


BrowserClicks = namedtuple('BrowserClicks', ['browser', 'total_clicks'])

class Browser():

	SQL_ALL_CLICKS = """
	SELECT 
		browser, COUNT(id) total_clicks
	FROM 
		url_shorter_click
	WHERE 
		link_id = %(link_id)s
	GROUP BY 
		browser;
	"""

	@classmethod
	def get_all_clicks(cls, link_id,):
		with connection.cursor() as cursor:
			cursor.execute(cls.SQL_ALL_CLICKS, {'link_id': link_id})
			rows=cursor.fetchall()
		return [BrowserClicks(*row) for row in rows]
	


	SQL_TIMEFRAME_CLICKS = """
	SELECT 
		browser, COUNT(id) total_clicks
	FROM 
		url_shorter_click
	WHERE 
		link_id = %(link_id)s AND 
		created_at >= %(timeframe_start)s AND
		created_at <  %(timeframe_end)s
	GROUP BY 
		browser;
	"""

	@classmethod
	def get_timeframe_clicks(cls, link_id, timeframe_start, timeframe_end):
		with connection.cursor() as cursor:
			cursor.execute(cls.SQL_TIMEFRAME_CLICKS, {'timeframe_start': timeframe_start, 'timeframe_end': timeframe_end, 'link_id': link_id})
			rows=cursor.fetchall()
		return [BrowserClicks(*row) for row in rows]


#DateClicks = namedtuple('DateClicks', ['datetime', 'total_clicks'])

PlatformClicks = namedtuple('PlatformClicks', ['platform', 'total_clicks'])

class Platform():

	SQL_ALL_CLICKS = """
	SELECT 
		platform, COUNT(id) total_clicks
	FROM 
		url_shorter_click
	WHERE 
		link_id = %(link_id)s
	GROUP BY 
		platform;
	"""

	@classmethod
	def get_all_clicks(cls, link_id,):
		with connection.cursor() as cursor:
			cursor.execute(cls.SQL_ALL_CLICKS, {'link_id': link_id})
			rows=cursor.fetchall()
		return [PlatformClicks(*row) for row in rows]
	


	SQL_TIMEFRAME_CLICKS = """
	SELECT 
		platform, COUNT(id) total_clicks
	FROM 
		url_shorter_click
	WHERE 
		link_id = %(link_id)s AND 
		created_at >= %(timeframe_start)s AND
		created_at <  %(timeframe_end)s
	GROUP BY 
		platform;
	"""

	@classmethod
	def get_timeframe_clicks(cls, link_id, timeframe_start, timeframe_end):
		with connection.cursor() as cursor:
			cursor.execute(cls.SQL_TIMEFRAME_CLICKS, {'timeframe_start': timeframe_start, 'timeframe_end': timeframe_end, 'link_id': link_id})
			rows=cursor.fetchall()
		return [PlatformClicks(*row) for row in rows]

StatsClicks = namedtuple('StatsClicks', ['browsers', 'platforms'])
