from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from .. import models

from django.contrib.auth import get_user_model
User = get_user_model()

def delete_all():
	User.objects.all().delete()
	models.Click.objects.all().delete()
	models.Link.objects.all().delete()

class CreateRandomData(APIView):
	permission_classes = (AllowAny,)
	def post(self, request,):
		delete_all()
		User.objects.create_user(email='test@mail.com', password='password', )
		link1 = models.Link(url='www.google.com', title='www.google.com')
		link1.save()
		link2 = models.Link(url='www.yahoo.com', title='www.yahoo.com')
		link2.save()
		link3 = models.Link(url='www.microsoft.com', title='www.microsoft.com')
		link3.save()
		return Response(status=status.HTTP_200_OK)

class DeleteRandomData(APIView):
	permission_classes = (AllowAny,)
	def delete(self, request,):
		delete_all()
		return Response(status=status.HTTP_200_OK)