from django.test import TestCase
from .. import models
from ..baseconv import base62 
from model_mommy import mommy
from django.contrib.auth import get_user_model
User = get_user_model()


class Base62ConverterTests(TestCase):
	def test_base62_to_decimal(self):
		a = base62.to_decimal('a')
		self.assertEqual(a,10)

	def test_decimal_to_base62(self):
		a = base62.from_decimal(62)
		self.assertEqual(a,'10')


from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status

class Useful(TestCase):
	def check_link(self, link):
		self.assertIn('full_url', link.keys()) 
		self.assertIn('short_url', link.keys()) 
		self.assertIn('created_at', link.keys()) 
		self.assertIn('title', link.keys()) 
		self.assertIn('href', link.keys()) 
		self.assertIn('id', link.keys())
		self.assertIn('total_clicks', link.keys())

	def check_link_stats(self, stats):
		self.assertIn('clicks', stats.keys()) 
		self.assertIn('platforms', stats.keys())

	def check_link_response(self, link_reponse, link_request):
		self.assertEqual(link_reponse.get('title'), link_request.get('url'))
		self.assertEqual(link_reponse.get('full_url'), link_request.get('url')) 

try:
	from unittest import mock, skip
except ImportError:
	import mock

from datetime import datetime, timedelta


class LinkListViewTests(TestCase):
	def setUp(self):
		self.client = APIClient()
		self.user = User.objects.create_user(email='test@example.com', password='password123', )
		self.client.login(username='test@example.com', password='password123')

	def test_create_link(self):
		request_item = {
			'url': 'www.google1.com',
		}
		response = self.client.post(reverse('api.link.list'), request_item)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.data)
		Useful().check_link(link=response.data)
		Useful().check_link_response(link_reponse=response.data, link_request=request_item)
		
		response = self.client.get(reverse('api.link.list'),)
		self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
		self.assertEqual(1,len(response.data), msg=response.data) 

	def test_get_links(self):

		links = mommy.make("url_shorter.Link", user=self.user, _quantity=2)
		response = self.client.get(reverse('api.link.list'),)
		self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
		self.assertEqual(len(links),len(response.data), msg=response.data) 
		Useful().check_link(link=response.data[0])

	def test_links_does_not_belong_to_user(self):
		users_links = mommy.make("url_shorter.Link", user=self.user, _quantity=3)
		others_links = mommy.make("url_shorter.Link", _quantity=2)
		response = self.client.get(reverse('api.link.list'),)
		self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
		self.assertEqual(len(users_links),len(response.data), msg=response.data) 
		Useful().check_link(link=response.data[0])



class LinkDetailViewTests(TestCase):
	def setUp(self):
		self.client =  APIClient()
		self.user = User.objects.create_user(email='test@example.com', password='password123', )
		self.link = mommy.make("url_shorter.Link", user = self.user)
		self.client.login(username='test@example.com', password='password123')
	
	def test_get_link(self):
		response = self.client.get(reverse('api.link.detail', args=[self.link.id]),)
		self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
		Useful().check_link(link=response.data)

	def test_not_found(self):
		response = self.client.get(reverse('api.link.detail', args=[121313]),)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND, msg=response.data)

	def test_link_does_not_belong_to_user(self):
		other_link = mommy.make("url_shorter.Link",)
		response = self.client.get(reverse('api.link.detail', args=[other_link.id]),)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND, msg=response.data)


import random
from django.utils import timezone


class LinkDetailStatsViewTests(TestCase):
	def setUp(self):
		self.client =  APIClient()
		self.user = User.objects.create_user(email='test@example.com', password='password123', )
		self.link = mommy.make("url_shorter.Link", user = self.user)
		self.link.save()
		self.client.login(username='test@example.com', password='password123')

		
		self.clicks = mommy.make("url_shorter.Click", link=self.link, _quantity=10)
		for click in self.clicks:
			created_at = timezone.now() - timedelta(days=random.randint(0, 10))
			click.created_at=created_at
			click.browser = random.randint(0, 4)
			click.save()
	
	def test_get_link_defaut_stats(self):
		response = self.client.get(reverse('api.link.detail.stats', args=[self.link.id]),)
		self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
		self.assertGreater(len(response.data['browsers']), 0, msg=response.data)


class ShorterRedirectViewTest(TestCase):
	def setUp(self):
		self.client =  APIClient()
		self.link = mommy.make("url_shorter.Link",)
	
	def test_get_link(self):
		response = self.client.get(reverse('shorter.redirect', args=[self.link.short_id]),)
		self.assertEqual(response.status_code, status.HTTP_301_MOVED_PERMANENTLY, )
		self.assertIn(self.link.url, response.get('location'),)
		clicks = models.Click.objects.filter(link=self.link)
		self.assertEqual(1, len(clicks))

class RamdonViewTests(TestCase):

	def setUp(self):
		self.client =  APIClient()
	
	def test_create_data(self):
		response = self.client.post(reverse('testing.create.data'))
		self.assertEqual(response.status_code, status.HTTP_200_OK, )
		links = models.Link.objects.all()
		self.assertEqual(len(links), 3, msg=links)
	
	def test_delete_data(self):
		links = mommy.make("url_shorter.Link", _quantity=2)
		response = self.client.delete(reverse('testing.delete.data'))
		self.assertEqual(response.status_code, status.HTTP_200_OK, )
		links = models.Link.objects.all()
		self.assertEqual(len(links), 0, msg=links)