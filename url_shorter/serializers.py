from rest_framework import serializers
from django.urls import reverse
from . import models
from .baseconv import base62 

class Link(serializers.ModelSerializer):
	href = serializers.SerializerMethodField()
	full_url = serializers.SerializerMethodField()
	short_url = serializers.SerializerMethodField()
	url = serializers.CharField(max_length=200, write_only=True)
	total_clicks = serializers.IntegerField(read_only=True)
	class Meta:
		model = models.Link
		fields = ('id', 'title', 'total_clicks', 'created_at','full_url', 'short_url', 'href', 'url',)
		extra_kwargs = {
			'title': {'required': False},
		}

	def get_href(self, obj):
	 	request = self.context.get('request', None)
	 	if request is not None:
	 		uri = request.build_absolute_uri(reverse('api.link.detail',args = [obj.id]))
	 	else:
	 		uri = reverse('api.link.detail', args = [obj.id])
	 	return uri

	def get_full_url(self, obj):
		return obj.url

	def get_short_url(self, obj):
	 	request = self.context.get('request', None)
	 	if request is not None:
	 		uri = request.build_absolute_uri(reverse('shorter.redirect',args = [obj.short_id]))
	 	else:
	 		uri = reverse('shorter.redirect', args = [obj.short_id])
	 	return uri
	
	def create(self, validated_data):
		url = validated_data['url']
		link = models.Link(url=url, title=url)
		link.save()
		return link



class BrowserClicks(serializers.Serializer):
	browser = serializers.SerializerMethodField()
	total_clicks = serializers.IntegerField()
	
	def get_browser(self, obj):
		return models.UserAgent.Browser(obj.browser).name.capitalize()


class PlatformClicks(serializers.Serializer):
	platform = serializers.SerializerMethodField()
	total_clicks = serializers.IntegerField()
	
	def get_platform(self, obj):
		return models.UserAgent.Platform(obj.platform).name.capitalize()

class StatsClicks(serializers.Serializer):
	browsers = BrowserClicks(many=True)
	platforms = PlatformClicks(many=True)