from django.shortcuts import render
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import pagination
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.permissions import AllowAny

from django.urls import reverse
from django.http import Http404
from . import serializers
from . import models
from django.db.models import Count

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect
from .baseconv import base62

class LinkList(APIView):

	def get_link(self, pk):
		try:
			return models.Link.objects.annotate(total_clicks=Count('clicks')).get(pk=pk)
		except ObjectDoesNotExist:
			raise Http404
	
	def get_links(self, user):
		return models.Link.objects.annotate(total_clicks=Count('clicks')).filter(user=user)

	def post(self, request,):
		serializer = serializers.Link(data=request.data,)
		if serializer.is_valid():
			# TODO: Make this one command insted of 2 
			serializer.save()
			serializer.instance.user = request.user
			serializer.instance.save()
			obj = self.get_link(serializer.instance.pk)
			serializer = serializers.Link(obj, context={'request':request} )
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


	def get(self, request,):
		links = self.get_links(user=request.user)
		serializer = serializers.Link(links, many=True, context={'request':request})
		return Response(serializer.data)

class LinkDetail(APIView):
	
	def get_link(self, pk, user):
		try:
			return models.Link.objects.annotate(total_clicks=Count('clicks')).get(pk=pk, user=user)
		except ObjectDoesNotExist:
			raise Http404

	def get(self, request, pk,):
		obj = self.get_link(pk, user=request.user)
		serializer = serializers.Link(obj, context={'request':request} )
		return Response(serializer.data)


from . import queries

class LinkDetailStats(APIView):
	
	def get_link(self, pk, user):
		try:
			return models.Link.objects.get(pk=pk, user=user)
		except ObjectDoesNotExist:
			raise Http404



	def get_all_stats(self, link_id):
		browser_stats = queries.Browser.get_all_clicks(link_id)
		platform_stats = queries.Platform.get_all_clicks(link_id)

		stats_clicks = queries.StatsClicks(browsers=browser_stats, platforms=platform_stats)
		return stats_clicks

	def get_timeframe_stats(self, link_id, ):
		browser_stats = queries.Browser.get_timeframe_clicks(link_id, timeframe_start, timeframe_end)
		stats_clicks = queries.StatsClicks(browsers=browser_stats)
		
		return stats_clicks


	def get(self, request, pk,):
		link = self.get_link(pk, user=request.user)
		stats_clicks = self.get_all_stats(pk)
		serializer = serializers.StatsClicks(stats_clicks, context={'request':request})
		return Response(serializer.data)



class ShorterRedirect(APIView):
	permission_classes = (AllowAny,)

	def get_ip_address(self, request):
		x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
		if x_forwarded_for:
			ip = x_forwarded_for.split(',')[0]
		else:
			ip = request.META.get('REMOTE_ADDR')
		return ip

	def get_platform(self, user_agent):
		if user_agent.is_mobile:
			return models.UserAgent.Platform.MOBILE
		elif user_agent.is_tablet:
			return models.UserAgent.Platform.TABLET
		elif user_agent.is_pc:
			return models.UserAgent.Platform.DESKTOP
		else:
			return models.UserAgent.Platform.UNKNOWN

	def get_browser(self, user_agent):
		if 'Firefox' in user_agent.browser.family:
			return models.UserAgent.Browser.FIREFOX

		elif 'Chrome' in user_agent.browser.family:
			return models.UserAgent.Browser.CHROME

		elif 'Safari' in user_agent.browser.family:
			return models.UserAgent.Browser.SAFARI

		elif 'Edge' in user_agent.browser.family or 'IE' in user_agent.browser.family:
			return models.UserAgent.Browser.IE_EDGE

		else:
			return models.UserAgent.Browser.UNKNOWN
	
	

	def get_os(self, user_agent):
		
		# Mobile
		if 'Android' == user_agent.os.family:
			return models.UserAgent.Os.ANDROID

		elif 'iOS' == user_agent.os.family:
			return models.UserAgent.Os.IOS
		
		# Desktop
		elif 'Windows' in user_agent.os.family:
			return models.UserAgent.Os.WINDOWS

		elif 'Mac OS X' == user_agent.os.family:
			return models.UserAgent.Os.MAC_OS_X

		elif user_agent.os.family in ('Linux','Debian','Ubuntu','Arch Linux','CentOS','Fedora','Gentoo','Mandriva','Linux Mint','Red Hat','Slackware','SUSE'):
			return models.UserAgent.Os.LINUX
		
		else:
			return models.UserAgent.Os.UNKNOWN


	def get_link(self, pk):
		try:
			return models.Link.objects.get(pk=pk)
		except ObjectDoesNotExist:
			raise Http404

	def get(self, request, base62_id,):
		id =  base62.to_decimal(base62_id)
		link = self.get_link(id)

		ip_address = self.get_ip_address(request)
		platform = self.get_platform(request.user_agent)
		browser = self.get_browser(request.user_agent)
		os = self.get_os(request.user_agent)
		click = models.Click(
			link=link,
			ip_address=ip_address,
			platform=platform,
			browser=browser,
			os=os,
		)
		click.save()

		link_url = link.url
		if not link_url.startswith("http"):
			link_url = 'http://'+ link_url
		return redirect(link_url, permanent=True)