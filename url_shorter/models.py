from django.db import models, connection
from .baseconv import base62
from django.utils import timezone
from datetime import timedelta
from django.conf import settings

# Query example: Link.objects.annotate(total_clicks=Count('clicks'))

class Link(models.Model):
	url = models.TextField()
	title = models.TextField()
	created_at = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='links', null=True, on_delete=models.SET_NULL,)

	@property
	def short_id(self):
		return base62.from_decimal(self.id)

import enum
class UserAgent():

	@enum.unique
	class Platform(enum.IntEnum):
		UNKNOWN = 0
		MOBILE = 1
		TABLET = 2
		PC = 3
		DESKTOP = 4

		@classmethod
		def choices(cls):
			return tuple([ (attr.value, attr.name,) for attr in cls])

	@enum.unique
	class Browser(enum.IntEnum):
		UNKNOWN = 0
		CHROME = 1
		FIREFOX = 2
		SAFARI = 3
		IE_EDGE = 4

		@classmethod
		def choices(cls):
			return tuple([ (attr.value, attr.name,) for attr in cls])

	@enum.unique
	class Os(enum.IntEnum):
		UNKNOWN = 0
		LINUX = 1
		WINDOWS = 2
		ANDROID = 3
		IOS = 4
		MAC_OS_X = 5

		@classmethod
		def choices(cls):
			return tuple([ (attr.value, attr.name,) for attr in cls])


class Click(models.Model):
	link = models.ForeignKey(Link, related_name='clicks', null=False, on_delete=models.CASCADE)
	platform = models.IntegerField(choices=UserAgent.Platform.choices(), default=UserAgent.Platform.UNKNOWN.value)
	browser = models.IntegerField(choices=UserAgent.Browser.choices(), default=UserAgent.Browser.UNKNOWN.value)
	os = models.IntegerField(choices=UserAgent.Os.choices(), default=UserAgent.Os.UNKNOWN.value)
	created_at = models.DateTimeField(default=timezone.now)
	ip_address = models.GenericIPAddressField(null=True, default=None)

	def __str__(self):
		return "{}:P:{}, {}".format(self.link.title, self.platform, self.created_at)