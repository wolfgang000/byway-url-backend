from django.urls import path
from django.conf import settings
from . import views

urlpatterns = [
	path('links/', views.LinkList.as_view(), name='api.link.list'),
	path('links/<int:pk>/', views.LinkDetail.as_view(), name='api.link.detail'),
	path('links/<int:pk>/stats/', views.LinkDetailStats.as_view(), name='api.link.detail.stats'),
]

if settings.ENVIRONMENT == 'development' or  settings.ENVIRONMENT == 'testing':
	from .tests import testing_view
	urlpatterns += [
		path('testing/create_data/', testing_view.CreateRandomData.as_view(), name='testing.create.data'),
		path('testing/delete_data/', testing_view.DeleteRandomData.as_view(), name='testing.delete.data'),
	]