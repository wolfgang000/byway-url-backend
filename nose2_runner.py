import nose2
import logging
import os
import sys
import argparse
#import config

if __name__ == '__main__':

	os.environ['DJANGO_SETTINGS_MODULE'] = 'byway_url.settings'

	import django
	from django.conf import settings
	from django.test.utils import get_runner

	django.setup()
	TestRunner = get_runner(settings)
	runner = TestRunner()
	nose2.discover(extraHooks=[('run_tests', runner)])
