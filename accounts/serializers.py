from rest_framework import serializers
from django.contrib.auth import password_validation
from django.core import exceptions
from . import models
from django.contrib.auth import get_user_model
User = get_user_model()



def password_validator(data):
	try:
		password_validation.validate_password(password=data)
	except exceptions.ValidationError as e:
		raise serializers.ValidationError(list(e.messages))
	return data

class UserRegister(serializers.ModelSerializer):
	password = serializers.CharField(write_only=True, required=True)
	class Meta:
		model = User
		fields = ('full_name', 'email', 'password',)
		extra_kwargs = {
			'full_name': {'required': True},
		}
	
	def validate_password(self, data):
		return password_validator(data)


class PasswordChange(serializers.Serializer):
	current_password = serializers.CharField(write_only=True, required=True)
	new_password = serializers.CharField(write_only=True, required=True)
	
	def validate_new_password(self, data):
		return password_validator(data)


class Login(serializers.Serializer):
	username = serializers.EmailField(write_only=True, required=True)
	password = serializers.CharField(write_only=True, required=True)

class PasswordReset(serializers.Serializer):
	email = serializers.EmailField(required=True)


class PasswordResetConfirm(serializers.Serializer):
	password = serializers.CharField(write_only=True, required=True)
	uid = serializers.CharField(write_only=True, required=True)
	token = serializers.CharField(write_only=True, required=True)

	def validate_password(self, data):
		try:
			password_validation.validate_password(password=data)
		except exceptions.ValidationError as e:
			raise serializers.ValidationError(list(e.messages))
		return data
