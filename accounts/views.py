from django.shortcuts import render
from django.contrib import auth
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import viewsets
from rest_framework import status
from rest_framework.views import APIView

from django.core.mail import EmailMessage
from django.template.loader import get_template

from django.contrib.auth import get_user_model
User = get_user_model()
from . import models, serializers


class Controller:

	def send_welcome_email(self, request, email_address):

		app_url = 'https://{}/'.format(settings.APP_HOST_NAME)

		ctx =  {
			'app_url': app_url
		}
		template = get_template('email/welcome.html')
		message = template.render(ctx, request)

		email = EmailMessage(
			subject = "Welcome to Byway!",
			body = message,
			to = [email_address]
		)
		email.content_subtype = 'html'
		email.send()

	def send_reset_password_email(self, request, email_address, ctx):
		template = get_template('email/password_reset.html')
		message = template.render(ctx, request)

		email = EmailMessage(
			subject = "Reset Password",
			body = message,
			to = [email_address]
		)
		email.content_subtype = 'html'
		email.send()

controller = Controller()

from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie
from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters


class Login(APIView):
	permission_classes = (AllowAny,)

	#@method_decorator(sensitive_post_parameters())
	def post(self, request,):
		serializer = serializers.Login(data=request.data)
		if serializer.is_valid():
			email = serializer.validated_data['username'].lower()
			user = auth.authenticate(
				request, 
				username = email, 
				password = serializer.validated_data['password']
			)
			if user is not None:
				auth.login(request, user)
				return Response(status=status.HTTP_200_OK)
			return Response(status=status.HTTP_400_BAD_REQUEST, )
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	@method_decorator(ensure_csrf_cookie)
	def get(self, request,):
		return Response(status=status.HTTP_200_OK)

class Logout(APIView):
	permission_classes = (AllowAny,)

	def get(self, request,):
		auth.logout(request)
		return Response(status=status.HTTP_200_OK)

class UserRegister(APIView):
	permission_classes = (AllowAny,)

	def post(self, request,):
		request_data = request.data.copy()
		if request_data.get('email') and isinstance(request_data.get('email'), str):
			request_data['email'] = request_data['email'].lower()
	
		serializer = serializers.UserRegister(data=request_data)
		if serializer.is_valid():
			user = User.objects.create_user(
				email = serializer.validated_data['email'], 
				password = serializer.validated_data['password'],
				full_name = serializer.validated_data['full_name']
			)
			controller.send_welcome_email(request, user.email)
			return Response(status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.conf import settings
from django.http import Http404

class PasswordChange(APIView):

	def post(self, request,):
		serializer = serializers.PasswordChange(data=request.data)
		if serializer.is_valid():
			if not auth.authenticate(request, username = request.user.email, password = serializer.validated_data['current_password']):
				errors = {
					'current_password':["The current password you entered was not correct"]
				}
				return Response(errors, status=status.HTTP_400_BAD_REQUEST)
			user = request.user
			user.set_password(serializer.validated_data['new_password'])
			user.save()
			return Response(status=status.HTTP_204_NO_CONTENT)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PasswordReset(APIView):
	permission_classes = (AllowAny,)

	def get_token(self, user):
		return default_token_generator.make_token(user)
	
	def get_uid(self, user):
		return urlsafe_base64_encode(force_bytes(user.pk)).decode()

	def post(self, request,):
		serializer = serializers.PasswordReset(data=request.data)
		if serializer.is_valid():
			email = serializer.validated_data['email'].lower()
			try:
				user = User.objects.get(email=email)
			except User.DoesNotExist:
				errors = {
					'email':["That email address doesn't match any user accounts. Are you sure you've registered?"]
				}
				return Response(errors, status=status.HTTP_404_NOT_FOUND)

			reset_link = 'https://{app_host_name}/#/account/password_reset/confirm/?uid={uid}&token={token}'.format(app_host_name = settings.APP_HOST_NAME, uid = self.get_uid(user), token = self.get_token(user),)
			context = {
				'reset_link': reset_link,
				'full_name': user.full_name,
			}
			controller.send_reset_password_email(request, email, context)
			return Response(status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


from django.http import Http404
from django.utils.http import is_safe_url, urlsafe_base64_decode
from django.core.exceptions import ValidationError

class PasswordResetConfirm(APIView):
	permission_classes = (AllowAny,)

	def get_user(self, uidb64):
		try:
			# urlsafe_base64_decode() decodes to bytestring
			uid = urlsafe_base64_decode(uidb64).decode()
			user = User.objects.get(pk=uid)
		except (TypeError, ValueError, OverflowError, User.DoesNotExist, ValidationError):
			user = None
		return user

	#@method_decorator(sensitive_post_parameters())
	def post(self, request,):
		serializer = serializers.PasswordResetConfirm(data=request.data)
		if serializer.is_valid():
			token = serializer.validated_data['token']
			uid = serializer.validated_data['uid']
			user = self.get_user(uid)
			if user is None:

				return Response({"uid": "The user doesn't exist" }, status=status.HTTP_400_BAD_REQUEST)
			if default_token_generator.check_token(user, token):
				user.set_password(serializer.validated_data['password'])
				user.save()			
				return Response(status=status.HTTP_200_OK)
			return Response({"token": "The reset code is invalid or has expired. Please try again"}, status=status.HTTP_400_BAD_REQUEST)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
