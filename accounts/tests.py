from django.test import TestCase, RequestFactory
from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
User = get_user_model()


from requests.auth import HTTPBasicAuth

class LoginAndLogoutViewTests(TestCase):
	USERNAME = 'test@example.com'
	PASSWORD = 'password123'
	def setUp(self):
		self.client = APIClient()
		self.user = User.objects.create_user(email=self.USERNAME, password=self.PASSWORD, )

	def test_login(self):
		request_body = {
			'username': self.USERNAME,
			'password': self.PASSWORD,
		}
		response = self.client.post(reverse('accounts.login'), request_body)
		self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

	def test_token_login(self):
		self.client.login(username=self.USERNAME, password=self.PASSWORD)
		response = self.client.post(reverse('knox_login'),)
		self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
		self.assertIn('token',response.data.keys())

	def test_token_logout(self):
		self.client.login(username=self.USERNAME, password=self.PASSWORD)
		response = self.client.post(reverse('knox_login'),)
		token = response.data['token']
		self.client.credentials(HTTP_AUTHORIZATION='Token ' +  token)

		response = self.client.post(reverse('knox_logout'))
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, msg=response.data)

	def test_login_case_insensitive(self):
		request_body = {
			'username': self.USERNAME.lower(),
			'password': self.PASSWORD,
		}
		response = self.client.post(reverse('accounts.login'), request_body)
		self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

		request_body = {
			'username': self.USERNAME.upper(),
			'password': self.PASSWORD,
		}
		response = self.client.post(reverse('accounts.login'), request_body)
		self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)



class SignupViewTests(TestCase):
	EMAIL = 'test@example.com'
	PASSWORD = 'password123'

	def setUp(self):
		self.client = APIClient()

	def test_signup(self):
		request_body = {
			'full_name': 'test user!',
			'email': self.EMAIL,
			'password': self.PASSWORD,
		}
		response = self.client.post(reverse('accounts.signup'), request_body)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED , msg=response.data)
		self.assertEqual(len(mail.outbox), 1)

	def test_signup_duplicate_email(self):
		request_body = {
			'full_name': 'test user!',
			'email': self.EMAIL,
			'password': self.PASSWORD,
		}
		response = self.client.post(reverse('accounts.signup'), request_body)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED , msg=response.data)

		response = self.client.post(reverse('accounts.signup'), request_body)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST , msg=response.data)
	

	def test_signup_case_insensitive(self):
		request_body = {
			'full_name': 'test user!',
			'email': self.EMAIL.lower(),
			'password': self.PASSWORD,
		}
		response = self.client.post(reverse('accounts.signup'), request_body)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED , msg=response.data)
	
		request_body = {
			'full_name': 'test user!',
			'email': self.EMAIL.upper(),
			'password': self.PASSWORD,
		}
		response = self.client.post(reverse('accounts.signup'), request_body)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, msg=response.data)


	def test_invalid_password(self):
		request_body = {
			'full_name': 'test user!',
			'email': self.EMAIL,
			'password': 'abc123',
		}
		response = self.client.post(reverse('accounts.signup'), request_body)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, msg=response.data)


class PasswordResetViewTests(TestCase):
	EMAIL = 'test@example.com'
	PASSWORD = 'password123'

	def setUp(self):
		self.client = APIClient()
		self.user = User.objects.create_user(email=self.EMAIL, password=self.PASSWORD, )

	def test_password_reset(self):
		request_body = {
			'email': self.EMAIL,
		}
		response = self.client.post(reverse('accounts.password_reset'), request_body)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED , msg=response.data)
		self.assertEqual(len(mail.outbox), 1)
		#TODO: check reset url

	def test_password_reset_fake_email(self):
		request_body = {
			'email': 'fake@example.com',
		}
		response = self.client.post(reverse('accounts.password_reset'), request_body)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND, msg=response.data)



from .views import PasswordReset

class PasswordResetConfirmViewTests(TestCase):
	EMAIL = 'test@example.com'
	PASSWORD = 'password123'

	def setUp(self):
		self.client = APIClient()
		self.user = User.objects.create_user(email=self.EMAIL, password=self.PASSWORD, )

	def test_password_reset_confirm(self):
		token = PasswordReset().get_token(self.user)
		uid = PasswordReset().get_uid(self.user)
		request_body = {
			'uid': uid,
			'token': token,
			'password': 'hello-my-baby',
		}
		response = self.client.post(reverse('accounts.password_confirm'), request_body)
		self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

	def test_invalid_token(self):
		token = 'aaabbbccc'
		uid = PasswordReset().get_uid(self.user)
		request_body = {
			'uid': uid,
			'token': token,
			'password': 'hello-my-baby',
		}
		response = self.client.post(reverse('accounts.password_confirm'), request_body)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, msg=response.data)

	def test_invalid_user(self):
		token = PasswordReset().get_token(self.user)
		request_body = {
			'uid': 'ssssaaaa',
			'token': token,
			'password': 'hello-my-baby',
		}
		response = self.client.post(reverse('accounts.password_confirm'), request_body)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, msg=response.data)

	def test_invalid_password(self):
		token = PasswordReset().get_token(self.user)
		uid = PasswordReset().get_uid(self.user)
		request_body = {
			'uid': uid,
			'token': token,
			'password': 'aabb',
		}
		response = self.client.post(reverse('accounts.password_confirm'), request_body)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, msg=response.data)


class PasswordChangeViewTests(TestCase):
	EMAIL = 'test@example.com'
	PASSWORD = 'password123'

	def setUp(self):
		self.client = APIClient()
		self.user = User.objects.create_user(email=self.EMAIL, password=self.PASSWORD, )
		self.client.login(username=self.EMAIL, password=self.PASSWORD)


	def test_change_password(self):
		request_body = {
			'current_password': self.PASSWORD,
			'new_password': 'hello-my-baby',
		}
		response = self.client.post(reverse('accounts.password_change'), request_body)
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, msg=response.data)


	def test_invalid_new_password(self):
		request_body = {
			'current_password': self.PASSWORD,
			'new_password': 'aabb',
		}
		response = self.client.post(reverse('accounts.password_change'), request_body)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, msg=response.data)

	def test_invalid_new_password(self):
		request_body = {
			'current_password': 'fake_old_password',
			'new_password': 'hello-my-baby',
		}
		response = self.client.post(reverse('accounts.password_change'), request_body)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, msg=response.data)



from model_mommy import mommy
from django.core import mail
from .views import controller

class SendEmailsTests(TestCase):
	def setUp(self):
		self.user = mommy.make("accounts.User",)
		self.factory = RequestFactory()

	def test_send_welcome(self):
		request = self.factory.get(reverse('accounts.logout'))

		controller.send_welcome_email(request, self.user)
		self.assertEqual(len(mail.outbox), 1)