from django.urls import path, include, re_path
from django.conf import settings
from . import views

urlpatterns = [
	#path('', include('django.contrib.auth.urls')),
	path('signup/', views.UserRegister.as_view(), name='accounts.signup'),
	path('login/', views.Login.as_view(), name='accounts.login'),
	path('logout/', views.Logout.as_view(), name='accounts.logout'),
	path('token/', include('knox.urls')),
	path('password_reset/', views.PasswordReset.as_view(), name='accounts.password_reset'),
	path('password_change/', views.PasswordChange.as_view(), name='accounts.password_change'),
	path('password_reset/confirm/', views.PasswordResetConfirm.as_view(), name='accounts.password_confirm'),
]