
# Dependencies

```
apt-get install postgresql libpq-dev python3-dev
```

# Setup dev environment

## Create databases

```
CREATE USER test_user WITH PASSWORD 'test_password';
CREATE database test_database;
GRANT ALL PRIVILEGES ON DATABASE "test_database" to test_user;


CREATE USER byway_user WITH PASSWORD 'byway_password';
CREATE database byway_dev;
GRANT ALL PRIVILEGES ON DATABASE "byway_dev" to byway_user;
```